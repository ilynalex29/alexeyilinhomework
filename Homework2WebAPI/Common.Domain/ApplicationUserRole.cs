﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Domain
{
    public class ApplicationUserRole
    {
        public int id { get; set; }
        public string name { get; set; } = default!;
        public IEnumerable<ApplicationUserApplicationRole> Users { get; set; }
    }
}
