﻿namespace Common.Domain
{
    public class Todo
    {
        public int Id { get; set; } = 0;
        public int OwnerID { get; set; } = 0;
        public string Label { get; set; } = null!;
        public ApplicationUser ApplicationUser { get; set; } = null!;
        public bool IsDone { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.MinValue;
        public DateTime UpdatedTime { get; set; } = DateTime.MinValue;
    }
}
