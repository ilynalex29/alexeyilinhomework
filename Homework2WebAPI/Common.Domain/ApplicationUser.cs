﻿namespace Common.Domain
{
    public class ApplicationUser
    {
        public int Id { get; set; } = 0;
        public string Login { get; set; } = default!;
        public string PasswordHash { get; set; } = default!;
        public IEnumerable<ApplicationUserApplicationRole> Roles { get; set; } = default!;
        public ICollection<Todo> todos { get; set; } = default!;
    }

  
}
