﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.API
{
    public static class ExceptionsHandlerMiddlewareExtensons
    {
        public static IApplicationBuilder UseExceptionsHandler(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionsHandlerMiddleware>();
            return app;
        }
    }
}
