﻿
using Common.Application.Exceptions;
using Microsoft.AspNetCore.Http;
using System.Net;
using System.Text.Json;

namespace Common.API
{
    public class ExceptionsHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        public ExceptionsHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            var statusCode = HttpStatusCode.InternalServerError;
            var result = string.Empty;
            try
            {
                await _next.Invoke(httpContext);
            }
            catch (Exception ex) 
            {
                switch (ex)
                {
                    case NotFoundException notFoundException:
                        statusCode = HttpStatusCode.NotFound;
                        result = JsonSerializer.Serialize(notFoundException.Message);
                        break;
                    case BadRequestException badRequestException:
                        statusCode = HttpStatusCode.BadRequest;
                        result = JsonSerializer.Serialize(badRequestException.Message);
                        break;
                    case ForbiddenException forbiddenException:
                        statusCode = HttpStatusCode.Forbidden;
                        result = JsonSerializer.Serialize(forbiddenException.Message);
                        break;
                }

                if (string.IsNullOrEmpty(result))
                {
                    result = JsonSerializer.Serialize(new {error = ex.Message,innerMessage = ex.InnerException?.Message, ex.StackTrace});
                }

                httpContext.Response.StatusCode = (int)statusCode;
                httpContext.Response.ContentType = "application/json";
                httpContext.Response.WriteAsync(result);
            }
        }
    }
}
