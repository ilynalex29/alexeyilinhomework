﻿
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Common.Domain;
using AutoMapper;
using Todos.Application.Query.GetListTodo;
using MediatR;
using Todos.Application.Command.SetIsDone;
using Todos.Application.Query.GetByIdTodo;
using Todos.Application.Command.CreateTodo;
using Todos.Application.Command.UpdateTodo;
using Todos.Application.Command.DeleteTodo;




namespace Homework2WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ToDoController : ControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> Get(
            GetListQuery getListQuery, 
            Mediator mediator,
            CancellationToken cancellationToken)
        {
            var result = await mediator.Send(getListQuery, cancellationToken);  
            return Ok(result);
        }

        [Route("{id}/IsDone")]
        [HttpPatch]
        public async Task<IActionResult> Patch(
            SetIsDoneCommand setIsDoneCommand,
            Mediator mediator,
            CancellationToken cancellationToken)
        {
            var result = await mediator.Send(setIsDoneCommand, cancellationToken);
            return Ok(result);
        }

        [HttpGet ("{id}")]
        public async Task<IActionResult> Get(
            GetByIdQuery getByIdQuery,
            Mediator mediator,
            CancellationToken cancellationToken)
        {
            var result = await mediator.Send(getByIdQuery, cancellationToken);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Post(
            CreateTodoCommand createTodoCommand,
            Mediator mediator,
            CancellationToken cancellationToken)
        {
            var createdTodo = mediator.Send(createTodoCommand, cancellationToken);
            return Created(uri: $"todos/{createdTodo.Id}", createdTodo);
        }

        [HttpPut]
        public async Task<IActionResult> Put(
            UpdateTodoCommand updateTodoCommand, 
            Mediator mediator,
            CancellationToken cancellationToken)
        {
            var updatedTodo = await mediator.Send(updateTodoCommand, cancellationToken);
            if (updatedTodo==null)
            {
                return NotFound();
            }
            else
            {
                return Ok(updatedTodo);
            }
        }


        [HttpDelete]
        public async Task<IActionResult> Delete(
            DeleteTodoCommand deleteTodoCommand,
            Mediator mediator,
            CancellationToken cancellationToken)
        {
            await mediator.Send(deleteTodoCommand, cancellationToken);
            return Ok();
        }
    }
}
