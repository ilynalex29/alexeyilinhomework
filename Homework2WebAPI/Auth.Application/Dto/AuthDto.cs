﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auth.Application.Dto
{
    public class AuthDto
    {
        public string login { get; set; } = default!;
        public string password { get; set; } = default!;
    }
}
