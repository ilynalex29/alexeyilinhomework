﻿using Auth.Appication.Dto;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auth.Application.Query.GetJwtToken
{
    public class GetJwtTokenQuery : IRequest<JwtTokenDto>
    {
        public string login { get; set; } = default!;
        public string password { get; set; } = default!;
    }
}
