﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auth.Application.Query.GetJwtToken
{
    public class GetJwtTokenQueryValidator : AbstractValidator<GetJwtTokenQuery>
    {
        public GetJwtTokenQueryValidator() 
        {
            RuleFor(e => e.login).MinimumLength(1);
            RuleFor(e => e.password).MinimumLength(1);
        }

    }
}
