﻿using Auth.Appication.Dto;
using Common.Application;
using Common.Application.Exceptions;
using Common.Domain;
using Common.Repositories;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using System.IdentityModel.Tokens.Jwt;
using MediatR;

namespace Auth.Application.Query.GetJwtToken
{
    public class GetJwtTokenQueryHandler : IRequestHandler<GetJwtTokenQuery, JwtTokenDto>
    {
        private readonly IRepository<ApplicationUser> _applicationUserRepository;
        private readonly IConfiguration _configuration;
        private readonly IRepository<RefreshToken> _refreshTokenRepository;
        public GetJwtTokenQueryHandler(IRepository<ApplicationUser> applicationUserRepository, IRepository<RefreshToken> refreshTokenRepository,IConfiguration configuration) 
        {
            _applicationUserRepository= applicationUserRepository;
            _refreshTokenRepository= refreshTokenRepository;
            _configuration = configuration;
        }

        public async Task<JwtTokenDto> Handle(GetJwtTokenQuery request, CancellationToken cancellationToken)
        {
            var user = await _applicationUserRepository.SingleOrDefaultAsync(u => u.Login == request.login, cancellationToken);
            if (user is null)
            {
                throw new NotFoundException($"User with login {request.login} not exists");
            }


            if (!PasswordHasher.VerifyPassword(request.password, user.PasswordHash))
            {
                new ForbiddenException();
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name,request.login),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
            };

            foreach (var role in user.Roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role.ApplicationUserRole.name));
            }

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]!));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);
            var dateExpires = DateTime.UtcNow.AddMinutes(5);
            var tokenDescriptor = new JwtSecurityToken(_configuration["Jwt:Issuer"]!, _configuration["Jwt:Audience"]!, claims, DateTime.UtcNow.AddMinutes(5), signingCredentials: credentials);
            var token = new JwtSecurityTokenHandler().WriteToken(tokenDescriptor);

            var refreshToken = await _refreshTokenRepository.AddAsync(new RefreshToken() { ApplicationUserId = user.Id }, cancellationToken);

            return new JwtTokenDto()
            {
                JwtToken = token,
                Expires = dateExpires,
                RefreshToken = refreshToken.Id
            };
        }

      
    }
}
