﻿using Auth.Application.Query.GetJwtToken;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auth.Application.Query.GetJwtTokenByRefreshToken
{
    public class GetJwtTokenByRefreshTokenValidator : AbstractValidator<GetJwtTokenByRefreshTokenQuery>
    {
        public GetJwtTokenByRefreshTokenValidator()
        {
            RuleFor(e=>e.refreshToken).MinimumLength(1);
        }
    }
}
