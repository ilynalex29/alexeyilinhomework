﻿using Auth.Appication.Dto;
using Common.Application.Exceptions;
using Common.Domain;
using Common.Repositories;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Auth.Application.Query.GetJwtTokenByRefreshToken
{
    public class GetJwtTokenByRefreshTokenHandler : IRequestHandler<GetJwtTokenByRefreshTokenQuery, JwtTokenDto>
    {
        private readonly IRepository<ApplicationUser> _applicationUserRepository;
        private readonly IConfiguration _configuration;
        private readonly IRepository<RefreshToken> _refreshTokenRepository;
        public GetJwtTokenByRefreshTokenHandler(IRepository<ApplicationUser> applicationUserRepository, IRepository<RefreshToken> refreshTokenRepository, IConfiguration configuration)
        {
            _applicationUserRepository = applicationUserRepository;
            _refreshTokenRepository = refreshTokenRepository;
            _configuration = configuration;
        }

        public async Task<JwtTokenDto> Handle(GetJwtTokenByRefreshTokenQuery request, CancellationToken cancellationToken)
        {
            var refreshTokenFromDB = await _refreshTokenRepository.SingleOrDefaultAsync(e => e.Id == request.refreshToken, cancellationToken);
            if (refreshTokenFromDB is null)
            {
                throw new ForbiddenException();
            }

            var user = await _applicationUserRepository.SingleAsync(u => u.Id == refreshTokenFromDB.ApplicationUserId, cancellationToken);

            var claims = new List<Claim>
            {
                new Claim( ClaimTypes.Name,user.Login),
                new Claim( ClaimTypes.NameIdentifier, user.Id.ToString()),
            };

            foreach (var role in user.Roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role.ApplicationUserRole.name));
            }

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]!));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);
            var dateExpires = DateTime.UtcNow.AddMinutes(5);
            var tokenDescriptor = new JwtSecurityToken(_configuration["Jwt:Issuer"]!, _configuration["Jwt:Audience"]!, claims, DateTime.UtcNow.AddMinutes(5), signingCredentials: credentials);
            var token = new JwtSecurityTokenHandler().WriteToken(tokenDescriptor);

            return new JwtTokenDto()
            {
                JwtToken = token,
                Expires = dateExpires,
                RefreshToken = refreshTokenFromDB.Id
            };
        }

    }
}
