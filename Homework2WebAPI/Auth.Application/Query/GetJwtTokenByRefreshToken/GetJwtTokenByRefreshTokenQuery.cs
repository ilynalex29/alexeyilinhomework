﻿using Auth.Appication.Dto;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auth.Application.Query.GetJwtTokenByRefreshToken
{
    public class GetJwtTokenByRefreshTokenQuery : IRequest<JwtTokenDto>
    {
        public string refreshToken { get; set; } = default!;
    }
}
