﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Domain;
using System.Data;
using System.Reflection.Emit;


namespace Common.Domain
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Todo> Todos { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<ApplicationUserRole> ApplicationUserRoles { get; set; }
        public DbSet<ApplicationUserApplicationRole> UserRoles { get; set; }
        public DbSet<RefreshToken> RefreshToken { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> _dbContextOptions) : base(_dbContextOptions)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Todo>().HasKey(c => c.Id);
            modelBuilder.Entity<Todo>().Property(b => b.Label).HasMaxLength(100).IsRequired();

            modelBuilder.Entity<ApplicationUser>().HasKey(u => u.Id);
            modelBuilder.Entity<ApplicationUser>().Property(b => b.Login).HasMaxLength(50).IsRequired();
            modelBuilder.Entity<ApplicationUser>().HasIndex(e => e.Login).IsUnique();
            modelBuilder.Entity<ApplicationUser>().Navigation(e=>e.Roles).AutoInclude();

            modelBuilder.Entity<RefreshToken>().HasKey(u => u.Id);
            modelBuilder.Entity<RefreshToken>().Property(u => u.Id).HasDefaultValueSql("NEWID()");
            modelBuilder.Entity<RefreshToken>().HasOne(e => e.applicationUser)
                .WithMany()
                .HasForeignKey(e => e.ApplicationUserId);

            modelBuilder.Entity<ApplicationUserApplicationRole>().HasKey(c => new
            {
                c.ApplicationUserId,
                c.ApplicationUserRoleId
            });

            modelBuilder.Entity<ApplicationUserApplicationRole>().Navigation(e => e.ApplicationUserRole).AutoInclude();

            modelBuilder.Entity<ApplicationUser>().HasMany(e => e.Roles)
                .WithOne(e => e.ApplicationUser)
                .HasForeignKey(e => e.ApplicationUserId);

            modelBuilder.Entity<ApplicationUserRole>().HasMany(e => e.Users)
                .WithOne(e => e.ApplicationUserRole)
                .HasForeignKey(e => e.ApplicationUserRoleId);

            modelBuilder.Entity<Todo>().HasOne(v => v.ApplicationUser)
                .WithMany(c => c.todos)
                .HasForeignKey(v => v.OwnerID);

            modelBuilder.Entity<ApplicationUserRole>().HasKey(u => u.id);
            modelBuilder.Entity<ApplicationUserRole>().Property(b => b.name).HasMaxLength(50).IsRequired();

            base.OnModelCreating(modelBuilder);
        }
    }
}
