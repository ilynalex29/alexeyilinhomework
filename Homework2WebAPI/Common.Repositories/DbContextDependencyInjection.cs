﻿using Common.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Common.Repositories
{
    public static class DbContextDependencyInjection
    {
        public static IServiceCollection AddTodosDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DbContext, ApplicationDbContext>(
                options=>
                {
                    options.UseSqlServer(configuration.GetConnectionString("SQLConnectionString"));
                }
                );
            services.AddTransient<IRepository<ApplicationUser>, SQLServerBaseRepository<ApplicationUser>>();
            services.AddTransient<IRepository<ApplicationUserRole>, SQLServerBaseRepository<ApplicationUserRole>>();
            return services;
        }
    }
}
