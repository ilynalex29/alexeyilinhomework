﻿
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Auth.Application.Query.GetJwtToken;
using MediatR;
using Auth.Application.Query.GetJwtTokenByRefreshToken;

namespace Users.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class AuthController : ControllerBase
    {
        [AllowAnonymous]
        [HttpPost("CreateJwtToken")]
        public async Task<IActionResult> Post(
            GetJwtTokenQuery getJwtTokenQuery,
            IMediator mediator,
            CancellationToken cancellationToken)
        {
            var createdJwtToken = await mediator.Send(getJwtTokenQuery, cancellationToken);
            return Ok(createdJwtToken);
        }

        [AllowAnonymous]
        [HttpPost("CreateJwtTokenByRefreshToken")]
        public async Task<IActionResult> Post(
            GetJwtTokenByRefreshTokenQuery getJwtTokenByRefreshTokenQuery, 
            Mediator mediator,
            CancellationToken cancellationToken)
        {
            var createdJwtToken = await mediator.Send(getJwtTokenByRefreshTokenQuery, cancellationToken); 
            return Ok(createdJwtToken);
        }
    }
}
