﻿using AutoMapper;
using Common.API.Exceptions;
using Common.Domain;
using Common.Repositories;
using System.Reflection.Metadata.Ecma335;
using Todos.Services.Dto;

namespace Todos.BL
{
    public class TodosService : ITodosService
    {
        private readonly IRepository<Todo> _todosRepository;
        private readonly IRepository<ApplicationUser> _usersRepository;
        private readonly IMapper _mapper;
        public TodosService(
            IRepository<Todo> todosRepository,
            IRepository<ApplicationUser> usersRepository,
            IMapper mapper)
        {
            _mapper = mapper;

            _todosRepository = todosRepository;
            _usersRepository = usersRepository;
        }
          



      
    }
}
