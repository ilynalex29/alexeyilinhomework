﻿using Common.Domain;
using Common.Repositories;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Todos.BL;


using Todos.Services.Mapping;
using Users.Services;

namespace Todos.Services
{
    public static class TodosServicesDependencyInjection
    {
        public static IServiceCollection AddTodosServices(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(AutoMapperProfile));

            services.AddTransient<ITodosService, TodosService>();
            services.AddTransient<IRepository<Todo>, SQLServerBaseRepository<Todo>>();

            services.AddTransient<IUsersService, UsersService>();
            services.AddTransient<IRepository<ApplicationUser>, SQLServerBaseRepository<ApplicationUser>>();

            services.AddValidatorsFromAssemblies(new[] { Assembly.GetExecutingAssembly() }, includeInternalTypes: true);

            return services;
        }
    }
}
