﻿
using Common.Domain;
using Todos.Services.Dto;

namespace Todos.BL
{
    public interface ITodosService
    {
        Task<Todo> GetByIDAsync(int id,CancellationToken cancellationToken = default);
        Task<IReadOnlyCollection<Todo>> GetListAsync(int? offset, int? limit,  string? LabelFreeText, CancellationToken cancellationToken = default);
        Task<Todo> CreateAsync(CreateTodosDto createTodoDto, CancellationToken cancellationToken = default);
        Task<Todo> UpdateAsync(UpdateTodosDto updateTodosDto, CancellationToken cancellationToken = default);
        Task<bool> DeleteAsync(int id, CancellationToken cancellationToken = default);
        Task<Todo> SetIsDoneAsync(PatchTodosDto patchTodosDto, CancellationToken cancellationToken = default);
    }
}