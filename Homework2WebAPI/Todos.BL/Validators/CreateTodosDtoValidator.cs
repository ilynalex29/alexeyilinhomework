﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using Todos.Services.Dto;

namespace Todos.Services.Validators
{
    public class CreateTodosDtoValidator:AbstractValidator<CreateTodosDto>
    {
        public CreateTodosDtoValidator() 
        {
            RuleFor(e=>e.OwnerID).GreaterThan(0).LessThan(int.MaxValue).WithMessage("Invalid OwnerID value");
            RuleFor(e => e.Label).MinimumLength(1).MaximumLength(200).WithMessage("Invalid Label text");
            RuleFor(e => e).NotNull().WithMessage("Data object cannot be null");
        }
    }
}
