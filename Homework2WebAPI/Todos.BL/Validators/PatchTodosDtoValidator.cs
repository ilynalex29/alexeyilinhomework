﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todos.Services.Dto;

namespace Todos.Services.Validators
{
    public class PatchTodosDtoValidator : AbstractValidator<PatchTodosDto>
    {
        public PatchTodosDtoValidator()
        {
            RuleFor(e => e.Id).GreaterThan(0).LessThan(int.MaxValue).WithMessage("Invalid OwnerID value");
            RuleFor(e => e).NotNull().WithMessage("Data object cannot be null");
        }
    }
}
