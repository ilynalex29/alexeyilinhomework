﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.Application.Dto;

namespace Users.Services.Query.GetCount
{
    public class GetCountQuery : BaseUserFilter, IRequest<int>
    {
    }
}
