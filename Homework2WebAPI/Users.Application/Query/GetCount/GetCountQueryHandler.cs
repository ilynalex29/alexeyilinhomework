﻿using Common.Domain;
using Common.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace Users.Services.Query.GetCount
{
    public class GetCountQueryHandler : IRequestHandler<GetCountQuery, int>
    {
        private readonly IRepository<ApplicationUser> _applicationUserRepository;

        public GetCountQueryHandler(IRepository<ApplicationUser> applicationUserRepository) 
        {
            _applicationUserRepository = applicationUserRepository;
        }

        public async Task<int> Handle(GetCountQuery request, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(request.nameFreeText))
            {
                return await _applicationUserRepository.CountAsync(cancellationToken: cancellationToken);
            }
            return await _applicationUserRepository.CountAsync(u => u.Login.Contains(request.nameFreeText), cancellationToken);
        }
    }
}
