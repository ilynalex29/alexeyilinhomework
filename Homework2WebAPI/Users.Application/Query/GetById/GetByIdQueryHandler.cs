﻿using AutoMapper;
using Common.Domain;
using Common.Repositories;
using MediatR;
using Users.Appication.Command.CreateUser;
using Users.Application.Dto;


namespace Users.Application.Query.GetById
{
    public class GetByIdQueryHandler : IRequestHandler<GetByIdQuery, GetUserDto>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<ApplicationUser> _applicationUserRepository;
        public GetByIdQueryHandler(IRepository<ApplicationUser> applicationUserRepository,IMapper mapper) 
        { 
            _mapper = mapper;
            _applicationUserRepository = applicationUserRepository;
        }

        public async Task<GetUserDto> Handle(GetByIdQuery request, CancellationToken cancellationToken)
        {
            return _mapper.Map<GetUserDto>(await _applicationUserRepository.SingleOrDefaultAsync(x => x.Id == request.id, cancellationToken));
        }
    }
}
