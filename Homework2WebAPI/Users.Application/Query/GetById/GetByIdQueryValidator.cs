﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Application.Query.GetById
{
    public class GetByIdQueryValidator : AbstractValidator<GetByIdQuery>
    {
        public GetByIdQueryValidator() 
        { 
            RuleFor(e=>e.id).LessThan(int.MaxValue).GreaterThanOrEqualTo(0);
        }
    }
}
