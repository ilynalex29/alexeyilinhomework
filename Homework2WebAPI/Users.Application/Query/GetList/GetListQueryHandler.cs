﻿using AutoMapper;
using Common.Domain;
using Common.Repositories;
using MediatR;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Users.Appication.Command.CreateUser;
using Users.Application;
using Users.Application.Dto;

namespace Users.Services.Query.GetList
{
    public class GetListQueryHandler : IRequestHandler<GetListQuery, IReadOnlyCollection<GetUserDto>>
    {
        private readonly IRepository<ApplicationUser> _applicationUserRepository;
        private readonly IMapper _mapper;
        private readonly MemoryCache _memoryCache;

        public GetListQueryHandler(IRepository<ApplicationUser> applicationUserRepository,UsersMemoryCache usersMemoryCache, IMapper mapper) 
        {
            _applicationUserRepository = applicationUserRepository;
            _mapper = mapper;
            _memoryCache = usersMemoryCache.Cache;
        }

        public async Task<IReadOnlyCollection<GetUserDto>> Handle(GetListQuery request, CancellationToken cancellationToken)
        {
            var  cacheKey = JsonSerializer.Serialize(request, new JsonSerializerOptions() { ReferenceHandler = ReferenceHandler.IgnoreCycles });
            if (_memoryCache.TryGetValue(cacheKey, out IReadOnlyCollection<GetUserDto>? result))
            {
                return result!;
            }

            result = _mapper.Map<IReadOnlyCollection<GetUserDto>>(await _applicationUserRepository.GetListAsync(
            request.offset,
            request.limit,
            request.nameFreeText == null ? null : u => u.Login.Contains(request.nameFreeText), u => u.Id,
            true,
            cancellationToken));

            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetAbsoluteExpiration(TimeSpan.FromMinutes(5))
                .SetSize(1);

            _memoryCache.Set(cacheKey, result, cacheEntryOptions);

            return result;
        }
    }
}
