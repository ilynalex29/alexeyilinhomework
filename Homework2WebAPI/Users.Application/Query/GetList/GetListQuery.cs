﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.Application.Dto;

namespace Users.Services.Query.GetList
{
    public class GetListQuery : BaseUserFilter, IRequest<IReadOnlyCollection<GetUserDto>>
    {
        public int? offset { get; set; }
        public int? limit { get; set; }
    }
}
