﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Services.Query.GetList
{
    public class GetListQueryValidator : AbstractValidator<GetListQuery>
    {
        public GetListQueryValidator() 
        {
            RuleFor(e => e.limit).GreaterThan(0).When(e => e.limit.HasValue);
            RuleFor(e => e.offset).GreaterThan(0).When(e => e.offset.HasValue);
            RuleFor(e => e.nameFreeText).MaximumLength(100);
        }
    }
}
