﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Application.Dto
{
    public class UpdateUsersDto
    {
        public int id { get; set; }
        public string login { get; set; } = default!;
        public string passwordHash { get; set; } = default!;
    }
}
