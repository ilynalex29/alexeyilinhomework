﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Application.Command.DeleteUser
{
    public class DeleteUserCommandValidator : AbstractValidator<DeleteUserCommand>
    {
        DeleteUserCommandValidator() 
        {
            RuleFor(e => e.id).GreaterThanOrEqualTo(0).LessThan(int.MaxValue);
        }
    }
}
