﻿using AutoMapper;
using Common.Application.Exceptions;
using Common.Domain;
using Common.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Application.Command.DeleteUser
{
    public class DeleteUserCommandHandler : IRequestHandler<DeleteUserCommand, bool>
    {
        private readonly IRepository<ApplicationUser> _applicationUserRepository;
        public DeleteUserCommandHandler(IRepository<ApplicationUser> applicationUserRepository)
        {
            _applicationUserRepository = applicationUserRepository;
        }

        public async Task<bool> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            var userEntity = await _applicationUserRepository.SingleOrDefaultAsync(b => b.Id == request.id, cancellationToken);
            if (userEntity == null)
            {
                throw new NotFoundException(new { id = request.id });
            }
            return await _applicationUserRepository.DeleteAsync(userEntity, cancellationToken);
        }

      
    }
}
