﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.Application.Dto;

namespace Users.Application.Command.UpdateUser
{
    public class UpdateUserCommand : IRequest<GetUserDto>
    {
        public int id { get; set; }
        public string login { get; set; } = default!;
        public string passwordHash { get; set; } = default!;
    }
}
