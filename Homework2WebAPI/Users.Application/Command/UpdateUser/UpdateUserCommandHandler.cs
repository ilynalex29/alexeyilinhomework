﻿using AutoMapper;
using Common.Application.Exceptions;
using Common.Domain;
using Common.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.Appication.Command.CreateUser;
using Users.Application.Dto;

namespace Users.Application.Command.UpdateUser
{
    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, GetUserDto>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<ApplicationUser> _applicationUserRepository;
        public UpdateUserCommandHandler(IRepository<ApplicationUser> applicationUserRepository,IMapper mapper) 
        {
            _applicationUserRepository = applicationUserRepository;
            _mapper = mapper;
        }

        public async Task<GetUserDto> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            if (await _applicationUserRepository.SingleOrDefaultAsync(b => b.Id == request.id, cancellationToken) == null)
            {
                throw new NotFoundException(new { id = request.id });
            }
            else
            {
                return _mapper.Map<GetUserDto>(await _applicationUserRepository.UpdateAsync(_mapper.Map<ApplicationUser>(new UpdateUsersDto() { id = request.id, login = request.login }), cancellationToken));
            }
        }
    }
}
