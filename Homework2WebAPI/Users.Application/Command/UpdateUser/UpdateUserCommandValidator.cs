﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.Application.Query.GetById;

namespace Users.Application.Command.UpdateUser
{
    public class UpdateUserCommandValidator : AbstractValidator<UpdateUserCommand>
    {
        public UpdateUserCommandValidator()
        {
            RuleFor(e=>e.passwordHash).MinimumLength(1).MaximumLength(1000);
            RuleFor(e=>e.id).GreaterThanOrEqualTo(0).LessThan(int.MaxValue);
            RuleFor(e=>e.login).MinimumLength(1).MaximumLength(1000);
        }
    }
}
