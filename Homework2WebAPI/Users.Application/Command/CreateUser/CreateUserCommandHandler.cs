﻿using AutoMapper;
using Common.Application;
using Common.Application.Exceptions;
using Common.Domain;
using Common.Repositories;
using MediatR;
using Users.Application.Dto;

namespace Users.Appication.Command.CreateUser
{
    public class CreateUserCommandHandler : IRequestHandler<CreateUsersCommand, GetUserDto>
    {
        private readonly IRepository<ApplicationUser> _users;
        private readonly IRepository<ApplicationUserRole> _roles;
        private readonly IMapper _mapper;
        public CreateUserCommandHandler(IRepository<ApplicationUser> users,IRepository<ApplicationUserRole> roles,IMapper mapper) 
        {
            _roles = roles;
            _users = users;   
            _mapper = mapper;
        }

        public async Task<GetUserDto> Handle(CreateUsersCommand request, CancellationToken cancellationToken)
        {
            if (await _users.SingleOrDefaultAsync(u => u.Login.Trim() == request.login.Trim()) is not null)
            {
                throw new BadRequestException("User login exists");
            }

            var userRole = await _roles.SingleOrDefaultAsync(r => r.name == "Admin", cancellationToken);

            var userEntity = new ApplicationUser
            {
                Login = request.login,
                PasswordHash = PasswordHasher.HashPassword(request.password),
                Roles = new[] { new ApplicationUserApplicationRole() { ApplicationUserRoleId = userRole.id } }
            };
            return _mapper.Map<GetUserDto>(await _users.AddAsync(userEntity, cancellationToken));
        }

        
    }
}
