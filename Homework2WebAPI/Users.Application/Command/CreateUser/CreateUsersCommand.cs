﻿using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.Application.Dto;

namespace Users.Appication.Command.CreateUser 
{
    public class CreateUsersCommand : IRequest<GetUserDto>
    {
        public string login { get; set; } = default!;
        public string password { get; set; } = default!;
    }
}
