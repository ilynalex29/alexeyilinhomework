﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Appication.Command.CreateUser
{
    public class CreateUserCommandValidator : AbstractValidator<CreateUsersCommand>
    {
        public CreateUserCommandValidator()
        {
            RuleFor(e => e.login).MinimumLength(1).MaximumLength(200).NotEmpty().WithMessage("Invalid login text");
            RuleFor(e => e.password).MinimumLength(5).MaximumLength(200).NotEmpty().WithMessage("Invalid password text");
            RuleFor(e => e).NotNull().WithMessage("Data object cannot be null");
        }
    }
}
