﻿using AutoMapper;
using Common.Domain;
using Users.Application.Dto;


namespace Users.Application.Mapping
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ApplicationUser, GetUserDto>();
            CreateMap<UpdateUsersDto, ApplicationUser>();
        }
    }
}
