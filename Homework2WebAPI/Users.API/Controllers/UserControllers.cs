﻿
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Users.Services;
using Common.Domain;
using AutoMapper;
using Users.Appication.Command.CreateUser;
using MediatR;
using Users.Application.Query.GetById;
using Users.Services.Query.GetList;
using Users.Services.Query.GetCount;
using Users.Application.Command.UpdateUser;
using Users.Application.Command.DeleteUser;

namespace Users.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserControllers : ControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> GetList(
            GetListQuery getListQuery, 
            Mediator mediator,
            CancellationToken cancellationToken)
        {
            var result = await mediator.Send(getListQuery, cancellationToken);
            var count = await mediator.Send(new GetCountQuery() { nameFreeText = getListQuery.nameFreeText }, cancellationToken); 
            HttpContext.Response.Headers.Append("X-TotalCount", count.ToString());
            return Ok(result);
        }

        
        [HttpGet ("{id}")]
        public async Task<IActionResult> Get(
            GetByIdQuery getByIdQuery, 
            Mediator mediator,
            CancellationToken cancellationToken)
        {
            var result = await mediator.Send(getByIdQuery, cancellationToken);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Post(
            CreateUsersCommand createUsersCommand,
            IMediator mediator,
            CancellationToken cancellationToken)
        {
            var createdUser = await mediator.Send(createUsersCommand, cancellationToken);
            return Created(uri: $"todos/{createdUser.Id}", createdUser);
        }

        [HttpPut]
        public async Task<IActionResult> Put(
            UpdateUserCommand updateUserCommand,
            Mediator mediator,
            CancellationToken cancellationToken)
        {
            var updatedTodo = await mediator.Send(updateUserCommand, cancellationToken);
            if (updatedTodo==null)
            {
                return NotFound();
            }
            else
            {
                return Ok(updatedTodo);
            }
        }


        [HttpDelete]
        public async Task<IActionResult> Delete(
            DeleteUserCommand deleteUserCommand,
            Mediator mediator,
            CancellationToken cancellationToken)
        {
            await mediator.Send(deleteUserCommand, cancellationToken);
            return Ok();
        }
    }
}
