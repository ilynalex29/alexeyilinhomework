﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todos.Application.Dto
{
    public class UpdateTodosDto
    {
        public int Id { get; set; } = 0;
        public int OwnerID { get; set; } = 0;
        public string Label { get; set; } = null!;
        public bool IsDone { get; set; }
    }
}
