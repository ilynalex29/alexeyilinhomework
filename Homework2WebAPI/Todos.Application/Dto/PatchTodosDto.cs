﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todos.Application.Dto
{
    public class PatchTodosDto
    {
        public int Id { get; set; } = 0;
        public bool isDone {  get; set; } = false;
    }
}
