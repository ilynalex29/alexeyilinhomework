﻿using AutoMapper;
using Common.Application.Exceptions;
using Common.Domain;
using Common.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Todos.Application.Dto;

namespace Todos.Application.Command.CreateTodo
{
    public class CreateTodoCommandHandler : IRequestHandler<CreateTodoCommand, Todo>
    {
        private readonly IRepository<Todo> _todoRepository;
        private readonly IMapper _mapper;
        public CreateTodoCommandHandler(IRepository<Todo> todoRepository, IMapper mapper)
        {
            _todoRepository = todoRepository;
            _mapper = mapper;
        }

        public async Task<Todo> Handle(CreateTodoCommand request, CancellationToken cancellationToken)
        {
            if (await _todoRepository.SingleOrDefaultAsync(b => b.Id == request.OwnerID, cancellationToken) == null)
            {
                throw new BadRequestException("Incorrect owner ID");
            }

            var todosEntity = _mapper.Map<CreateTodosDto, Todo>(new CreateTodosDto() { Label = request.Label, OwnerID = request.OwnerID });
            todosEntity.CreatedDate = todosEntity.UpdatedTime = DateTime.UtcNow;

            await _todoRepository.AddAsync(todosEntity, cancellationToken);
            return todosEntity;
        }
    }
}
