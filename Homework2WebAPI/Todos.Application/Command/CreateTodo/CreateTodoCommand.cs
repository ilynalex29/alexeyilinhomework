﻿using Common.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Todos.Application.Command.CreateTodo
{
    public class CreateTodoCommand : IRequest<Todo>
    {
        public int OwnerID { get; set; } = 0;
        public string Label { get; set; } = null!;
    }
}
