﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todos.Application.Command.CreateTodo
{
    public class CreateTodoCommandValidator : AbstractValidator<CreateTodoCommand>
    {
        public CreateTodoCommandValidator()
        {
            RuleFor(e => e.OwnerID).GreaterThanOrEqualTo(0).LessThan(int.MaxValue);
            RuleFor(e => e.Label).MinimumLength(1).MaximumLength(100);
        }
    }
}
