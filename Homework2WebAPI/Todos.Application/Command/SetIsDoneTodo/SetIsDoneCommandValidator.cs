﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todos.Application.Command.SetIsDone
{
    public class SetIsDoneCommandValidator : AbstractValidator<SetIsDoneCommand>
    {
        public SetIsDoneCommandValidator() 
        {
            RuleFor(e => e.id).GreaterThanOrEqualTo(0).LessThan(int.MaxValue);
        }
    }
}
