﻿using AutoMapper;
using Common.Application.Exceptions;
using Common.Domain;
using Common.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todos.Application.Dto;

namespace Todos.Application.Command.SetIsDone
{
    public class SetIsDoneCommandHandler : IRequestHandler<SetIsDoneCommand, Todo>
    {
        private readonly IRepository<Todo> _todoRepository;
        public SetIsDoneCommandHandler(IRepository<Todo> todoRepository)
        {
            _todoRepository = todoRepository;
        }

        public async Task<Todo> Handle(SetIsDoneCommand request, CancellationToken cancellationToken)
        {
            var todoEntity = await _todoRepository.SingleOrDefaultAsync(e => e.Id == request.id);
            if (todoEntity == null)
            {
                throw new NotFoundException(new { id = request.id });
            }
            todoEntity.UpdatedTime = DateTime.UtcNow;
            todoEntity.IsDone = true;
            return await _todoRepository.UpdateAsync(todoEntity, cancellationToken);
        }

        
    }
}
