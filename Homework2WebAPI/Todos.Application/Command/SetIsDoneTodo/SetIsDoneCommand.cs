﻿using Common.Domain;
using MediatR;

namespace Todos.Application.Command.SetIsDone
{
    public class SetIsDoneCommand : IRequest<Todo>
    {
        public int id { get; set; }
    }
}
