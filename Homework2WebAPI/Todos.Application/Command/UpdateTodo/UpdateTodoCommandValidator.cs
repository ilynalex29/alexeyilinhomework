﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todos.Application.Command.UpdateTodo
{
    public class UpdateTodoCommandValidator : AbstractValidator<UpdateTodoCommand>
    {
        public UpdateTodoCommandValidator() 
        {
            RuleFor(e => e.Id).LessThan(int.MaxValue).GreaterThanOrEqualTo(0);
            RuleFor(e => e.Label).MaximumLength(1000).MinimumLength(1);
            RuleFor(e=>e.OwnerID).LessThan(int.MaxValue).GreaterThanOrEqualTo(0);
        }
    }
}
