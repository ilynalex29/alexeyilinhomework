﻿using AutoMapper;
using Common.Application.Exceptions;
using Common.Domain;
using Common.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Todos.Application.Command.UpdateTodo
{
    public class UpdateTodoCommandHandler : IRequestHandler<UpdateTodoCommand, Todo?>
    {
        private readonly IRepository<Todo> _todoRepository;
        public UpdateTodoCommandHandler(IRepository<Todo> todoRepository, IMapper mapper) 
        {
            _todoRepository = todoRepository;
        }

        public async Task<Todo?> Handle(UpdateTodoCommand request, CancellationToken cancellationToken)
        {
            var todoEntity = await _todoRepository.SingleOrDefaultAsync(b => b.Id == request.Id, cancellationToken);
            if (todoEntity == null)
            {
                throw new BadRequestException("Incorrect owner ID");
            }

            todoEntity.UpdatedTime = DateTime.UtcNow;
            return await _todoRepository.UpdateAsync(todoEntity, cancellationToken);
        }
    }
}
