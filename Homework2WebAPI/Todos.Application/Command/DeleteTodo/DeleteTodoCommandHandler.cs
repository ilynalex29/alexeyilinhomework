﻿using Common.Application.Exceptions;
using Common.Domain;
using Common.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todos.Application.Command.DeleteTodo
{
    public class DeleteTodoCommandHandler : IRequestHandler<DeleteTodoCommand, bool>
    {
        private readonly IRepository<Todo> _todoRepository;
        public DeleteTodoCommandHandler(IRepository<Todo> todoRepository)
        {
            _todoRepository = todoRepository;
        }

        public async Task<bool> Handle(DeleteTodoCommand request, CancellationToken cancellationToken)
        {
            var todosToDelete = await _todoRepository.SingleOrDefaultAsync(e => e.Id == request.id);
            if (todosToDelete == null)
            {
                throw new NotFoundException(new { id = request.id });
            }

            return await _todoRepository.DeleteAsync(todosToDelete, cancellationToken);
        }

    }
}
