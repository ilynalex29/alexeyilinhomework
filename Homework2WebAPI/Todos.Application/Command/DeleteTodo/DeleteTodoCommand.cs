﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todos.Application.Command.DeleteTodo
{
    public class DeleteTodoCommand : IRequest<bool>
    {
        public int id {  get; set; }
    }
}
