﻿using AutoMapper;
using Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todos.Application.Dto;

namespace Todos.Application.Mapping
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile() 
        {
            CreateMap<UpdateTodosDto, Todo>();
            CreateMap<CreateTodosDto, Todo>();
            CreateMap<PatchTodosDto, Todo>();
        }
    }
}
