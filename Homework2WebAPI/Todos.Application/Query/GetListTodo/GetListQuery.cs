﻿using Common.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todos.Application.Query.GetListTodo
{
    public class GetListQuery : IRequest<IReadOnlyCollection<Todo>>
    {
        public string labelFreeText { get; set; } = default!;
        public int offset { get; set; }
        public int limit { get; set; }
    }
}
