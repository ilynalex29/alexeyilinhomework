﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todos.Application.Query.GetListTodo
{
    public class GetListQueryValidator : AbstractValidator<GetListQuery>
    {
        public GetListQueryValidator() 
        { 
            RuleFor(e=>e.labelFreeText).MaximumLength(1000).MinimumLength(1);
            RuleFor(e => e.offset).LessThan(int.MaxValue);
            RuleFor(e=>e.limit).LessThan(int.MaxValue);
        }
    }
}
