﻿using Common.Domain;
using Common.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todos.Application.Query.GetListTodo
{
    public class GetListQueryHandler : IRequestHandler<GetListQuery, IReadOnlyCollection<Todo>>
    {
        private readonly IRepository<Todo> _todoRepository;
        public GetListQueryHandler(IRepository<Todo> todoRepository)
        {
            _todoRepository = todoRepository;
        }

        public async Task<IReadOnlyCollection<Todo>> Handle(GetListQuery request, CancellationToken cancellationToken)
        {
            return await _todoRepository.GetListAsync(
                request.offset,
                request.limit,
                request.labelFreeText == null ? null : b => b.Label.Contains(request.labelFreeText),
                b => b.Id,
                true, cancellationToken);
        }
    }
}
