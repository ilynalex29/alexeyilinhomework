﻿using Common.Application.Exceptions;
using Common.Domain;
using Common.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todos.Application.Query.GetByIdTodo
{
    public class GetByIdQueryHandler : IRequestHandler<GetByIdQuery, Todo>
    {
        private readonly IRepository<Todo> _todoRepository;
        public GetByIdQueryHandler(IRepository<Todo> todoRepository) 
        {
            _todoRepository = todoRepository;
        }

        public async Task<Todo> Handle(GetByIdQuery request, CancellationToken cancellationToken)
        {
            var todo = await _todoRepository.SingleOrDefaultAsync(b => b.Id == request.id, cancellationToken);
            if (todo == null)
            {
                throw new NotFoundException(new { id = request.id });
            }
            return todo;
        }
    }
}
