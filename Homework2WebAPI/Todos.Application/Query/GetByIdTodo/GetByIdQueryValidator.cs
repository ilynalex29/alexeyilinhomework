﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todos.Application.Query.GetByIdTodo
{
    public class GetByIdQueryValidator : AbstractValidator<GetByIdQuery>
    {
        public GetByIdQueryValidator() 
        {
            RuleFor(e => e.id).GreaterThanOrEqualTo(0).LessThan(int.MaxValue);
        }
    }
}
