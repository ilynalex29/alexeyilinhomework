﻿using Common.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todos.Application.Query.GetByIdTodo
{
    public class GetByIdQuery : IRequest<Todo>
    {
        public int id {  get; set; }
    }
}
